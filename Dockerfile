FROM docker.io/immawanderer/alpine-hugo:hugo-v0.115.3 AS hugobuild

WORKDIR /docs

COPY . /docs

RUN git submodule init \
	&& git submodule update --recursive \
	&& hugo version \
	&& hugo --minify --gc=true


FROM docker.io/library/nginx:mainline-alpine
# COPY --from=hugobuild /home/$(whoami)/public/ /usr/share/nginx/html
COPY --from=hugobuild /docs/public/ /usr/share/nginx/html

# tripple slash reference
# https://stackoverflow.com/questions/5190966/using-sed-to-insert-tabs/5191165#5191165
RUN sed -i -e 's/^worker_processes  auto;/worker_processes auto;/'\
 -e "/^events {$/ a \\\tmulti_accept on;\n\tuse epoll;"\
 -e "/^http {$/ a \\\tserver_tokens off;\n\tetag off;\n"\
 -e 's/#tcp_nopush/tcp_nopush/'\
 -e "/tcp_nopush/ a \\\ttcp_nodelay on;"\
 -e "s/^    */$(printf '\t')/"\
 /etc/nginx/nginx.conf
