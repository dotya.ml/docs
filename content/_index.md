---
title: Introduction
type: docs
---

# Welcome to our docs!

This site is a place for the documentation of [dotya.ml](https://dotya.ml)'s services - partly telling us what we've set up but mainly advising the users *how* the services can be used, providing helpful tips along the way.

{{< hint info >}}
If you find something's missing or you just want to contribute, any **suggestions, patches or PRs are most welcome.**
{{< /hint >}}
