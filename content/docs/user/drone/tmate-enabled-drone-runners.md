---
title: "tmate-enabled Drone runners"
date: 2022-03-22T15:41:06+01:00
draft: false
---
#### tl;dr
The gist is that a pipeline *can* be (re)started (usually, only failed pipelines
are manually restarted, hoping for a better result as if the error was floating
somewhere around the Ether and not our code most of the times) in *debug* mode.

After a pipeline fails in said debug mode, a `tmate` client connection to a
[`tmate`](https://tmate.io/) server (as configured in the `drone` server
configuration files -
[ref](https://git.dotya.ml/dotya.ml/drone/commit/e6e2a0233e00874ecd859435aee76f0a01286063))
is open from withing the container that the failing *step* was running in and
connection instructions for `ssh`-ing are displayed.

The error can then be debugged more intimately in the precise environment that made the bug appear.

#### conclusion
Instead of repeating any more of what's already been said, I suggest anybody
interested reads the original
[kind-of-reference](https://discourse.drone.io/t/feature-preview-debug-mode/8344)
post by <abbr title="one of the authors of Drone CI">Brad</abbr>.
