---
weight: 10
title: gitea
---
# gitea

*Gitea user documentation*
{{< hint info >}}
**note**\
links to the [upstream project's original docs](https://docs.gitea.io) are provided wherever relevant so that we don't repeat the same things here
{{< /hint >}}
