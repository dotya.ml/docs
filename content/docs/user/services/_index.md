---
weight: 10
title: services
---
# list of services
* [gitea](https://git.dotya.ml/)
* [drone](https://drone.dotya.ml/)
* [prometheus](https://metrics.dotya.ml/)
* [grafana](https://grafana.dotya.ml/)
* [statping](https://status.dotya.ml/)
* DNSCrypt resolver (see https://dotya.ml/2021/08/dnscrypt-running-the-server/)
