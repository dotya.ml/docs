# [`docs`](https://git.dotya.ml/dotya.ml/docs/src/branch/master)

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Build Status](https://drone.dotya.ml/api/badges/dotya.ml/docs/status.svg)](https://drone.dotya.ml/dotya.ml/docs)

sawce of the dotya.ml's docs site.

Built with [Hugo](https://gohugo.io) on [Book]( https://github.com/alex-shpak/hugo-book) theme.
